package pl.waw.bitmed.italianovero;

public class MyFactory {
    public static final int TENSES = 0;
    public static final int PRENOMES = 1;

    public static MyInterface createObject(int type) {
        switch (type) {
            case TENSES:
                return new MenagerTense();
            case PRENOMES:
                return new MenagerPrenome();
                default:
                    return null;
        }
    }
}
