package pl.waw.bitmed.italianovero;

import java.util.Random;

public class MenagerPrenome implements MyInterface {
    private String[] prenomes = {"io", "tu", "lui", "lei", "Lui", "Lei", "Noi", "Voi", "Loro", "One"};

    @Override
    public String sort() {
        return prenomes[getRandomNumber()];
    }

    @Override
    public int getRandomNumber() {
        int min = 0;
        int max = prenomes.length - 1;

        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }
}
