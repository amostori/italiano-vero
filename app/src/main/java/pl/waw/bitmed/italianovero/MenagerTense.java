package pl.waw.bitmed.italianovero;

import java.util.Random;

public class MenagerTense implements MyInterface {
    private String[] times = {"Presente", "Imperativo", "Indicativo - pass. pross.", "Futuro", "Condizionale", "Imperfetto", "Pass. Rem."};
    @Override
    public String sort() {
        return times[getRandomNumber()];
    }

    @Override
    public int getRandomNumber() {
        int min = 0;
        int max = times.length - 1;

        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }
}
