package pl.waw.bitmed.italianovero;

import java.util.Random;

public class SortingClass implements SortingInterface {
    private String[] table;

    @Override
    public String chooseOne() {
        return table[randomNumber()];
    }

    @Override
    public int randomNumber() {
        int min = 0;
        int max = table.length - 1;

        Random r = new Random();
        return r.nextInt(max - min + 1) + min;
    }

    @Override
    public void setTable(String[] array) {
        table = array;
    }
}
