package pl.waw.bitmed.italianovero;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.buttonPrenome)
    Button buttonPrenome;
    @BindView(R.id.buttonTense)
    Button buttonTense;

    String[] prenomeTable;
    String[] tensesTable;
    SortingInterface prenomesSortingClass;
    SortingInterface tensesSortingClass;
    @BindView(R.id.spinner)
    AppCompatSpinner spinner;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("Italiano Vero");

        prenomeTable = getResources().getStringArray(R.array.prenomes);
        tensesTable = getResources().getStringArray(R.array.tenses);

        prenomesSortingClass = SortingFactory.createSortingClass(SortingFactory.SORTING_CLASS);
        prenomesSortingClass.setTable(prenomeTable);
        tensesSortingClass = SortingFactory.createSortingClass(SortingFactory.SORTING_CLASS);
        tensesSortingClass.setTable(tensesTable);
        setspinner();

    }

    private void setspinner() {
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.verbi, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        //spinner.setOnItemSelectedListener(this);

    }

    @OnClick({R.id.buttonPrenome, R.id.buttonTense})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.buttonPrenome:
                String sort1 = prenomesSortingClass.chooseOne();
                buttonPrenome.setText(sort1);
                break;
            case R.id.buttonTense:
                String sort = tensesSortingClass.chooseOne();
                buttonTense.setText(sort);
                break;
        }
    }
}
