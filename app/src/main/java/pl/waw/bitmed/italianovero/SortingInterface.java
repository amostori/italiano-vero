package pl.waw.bitmed.italianovero;

public interface SortingInterface {
    String chooseOne();

    int randomNumber();

    void setTable(String[] array);
}
