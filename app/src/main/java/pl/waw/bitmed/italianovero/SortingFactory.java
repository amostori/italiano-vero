package pl.waw.bitmed.italianovero;

public class SortingFactory {
    public static final int SORTING_CLASS = 0;

    public static SortingInterface createSortingClass(int type) {
        switch (type) {
            case SORTING_CLASS:
                return new SortingClass();
                default:
                    return null;
        }
    }

}
